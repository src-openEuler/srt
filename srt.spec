Name:           srt
Version:        1.4.4
Release:        2
Summary:        Secure Reliable Transport protocol tools

License:        MPLv2.0
URL:            https://www.srtalliance.org
Source0:        https://github.com/Haivision/srt/archive/v%{version}/%{name}-%{version}.tar.gz

BuildRequires:  cmake gcc-c++ make
BuildRequires:  gnutls-devel gmock-devel gtest-devel

Requires: srt-libs%{?_isa} = %{version}-%{release} ffmpeg

%description
Secure Reliable Transport (SRT) is an open source transport technology that
optimizes streaming performance across unpredictable networks, such as 
the Internet.

%package libs
Summary: Secure Reliable Transport protocol libraries

%description libs
Secure Reliable Transport protocol libraries

%package devel
Summary: Secure Reliable Transport protocol development libraries and headers
Requires: srt-libs%{?_isa} = %{version}-%{release}

%description devel
Secure Reliable Transport protocol development libraries and header files

%prep
%autosetup

%build
%cmake \
  -DENABLE_STATIC=OFF \
  -DENABLE_UNITTESTS=ON \
  -DENABLE_GETNAMEINFO=ON \
  -DUSE_ENCLIB=gnutls
%cmake_build


%install
%cmake_install
# remove old upstream temporary compatibility pc
rm -f %{buildroot}/%{_libdir}/pkgconfig/haisrt.pc


%check
# - test are broken on s390x for some slowness/timing reason
# - tests can't be run in parallel because they reuse port numbers
# - TestIPv6 are known broken due to v4_v6 mapping differnces between platforms
#   https://github.com/Haivision/srt/issues/1972#
%ifnarch s390x
%ctest -j1 -E TestIPv6
%endif

%files
%license LICENSE
%doc README.md docs
%{_bindir}/srt-ffplay
%{_bindir}/srt-file-transmit
%{_bindir}/srt-live-transmit
%{_bindir}/srt-tunnel

%files libs
%license LICENSE
%{_libdir}/libsrt.so.1*

%files devel
%doc examples
%{_includedir}/srt
%{_libdir}/libsrt.so
%{_libdir}/pkgconfig/srt.pc


%changelog
* Thu Nov 21 2024 Funda Wang <fundawang@yeah.net> - 1.4.4-2
- adopt to new cmake macro

* Thu Aug 10 2023 leeffo <liweiganga@uniontech.com> - 1.4.4-1
- upgrade to version 1.4.4

* Mon Sep 13 2021 weidong <weidong@uniontech.com> - 1.4.1-4
- Add installation dependencies

* Fri May 07 2021 weidong <weidong@uniontech.com> - 1.4.1-3
- Initial package.
